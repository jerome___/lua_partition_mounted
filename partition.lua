local function ismounted(model)                       -- this function is provate
  pattern = "^%S+%s+" .. model .. "%s+"                           -- search second column of file, and add two space chars sorrounding the partition for remove doubles (/mnt/test/mnt/test for exemple)
  for line in io.lines("/proc/self/mounts") do                    -- for each line of /proc/self/mountinfo 
    if string.find(line, pattern) then                            -- test if the pattern exist and...
      return true 			                          -- return true if it exist
    end
  end
  return false                                                    -- it should return flase only if no pattern has been found
end

return {                                              -- this is the table to return
  ismounted=ismounted                                -- it include the value returned from the private function
}
