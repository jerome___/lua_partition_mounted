#! /usr/bin/lua 

-- include the module with my function to control if a partition is mounted or not (in file partition.lua)
local partition = require "partition"

-- include 3rd party library LFS for be able to check file/partitions safe entry
local lfs = require "lfs"

-- get argumentthen check if exist and if it is a partition (care of safe entry code args)
if #arg == 0 then                                                 -- is there any argument ? 
  print([[
  you need to give any partition(s) in argument, it should be separate by space
  for exemple:
     partition_mounted /mnt/test
  or
     partition_mounted /mnt/test /mnt/STFP]])
  return
end

for i=1, #arg do                                                -- then check all of them one by one
  local mode = lfs.attributes(arg[i], 'mode')                   -- LFS check the entry argument
  if mode == nil then                                           -- if nil, that is because it does not exist at all.
    print(arg[i], "does not exist, so it is forbiden to test it")
  elseif mode ~= 'directory' then                               -- if LFS said it is not a directory...
    print(arg[i], "is not a directory, no test occur on that.")
  else                                                          -- check: partition is mounted ?
    print(arg[i], partition.ismounted(arg[i]) and "yes" or "no")
  end
end

