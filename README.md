# Lua - partition_mounted #

little code in lua for test if a partition is actually mounted.

### The idea ###

* a function from a module (partition.lua) has to return boolean conditional test (true or false) from a table
* this function is: ismounted(partition) return true if the string "partition" is mounted
* this works on linux only by show in second column of /proc/self/mounts proc system file if this partition exist and is mounted
* it can be use like an application for check if any partitions lists are mounted
* i use LuaFileSystem plugin for check arguments entries too be sure there is nothing bad
* i does it just for learn and use with conky/lua for show partitions information, but i add more than just the little function for playing.

### How do I get set up ? ###

* install lua (mine is version 5.3) and make it shown and usable by the system
* install luarocks
* with luarocks, install LuaFileSystem
* use the function code inside an other lua code
* use it from command line directly like a script with the string partition name as argument

### exemple ###

* ./partition_mounted /mnt/not_exist /home/toto /home/jerome/test_file.txt